package main

import (
	"context"
	"fmt"

	"mod/app/custom/service_linked_role"

	"github.com/aws/aws-lambda-go/cfn"
	"github.com/aws/aws-lambda-go/lambda"
)

func handleCustomResources(ctx context.Context, event cfn.Event) (physicalID string, data map[string]interface{}, err error) {
	switch event.ResourceType {
	case "Custom::ServiceLinkedRole":
		custom := service_linked_role.NewServiceLinkedRole(ctx, event)
		return custom.Handle()
	default:
		err = fmt.Errorf("unknown resource type %s", event.ResourceType)
	}
	return
}

func main() {
	lambda.Start(cfn.LambdaWrap(handleCustomResources))
}
