package service_linked_role

import (
	"context"

	"mod/app/custom"

	"github.com/aws/aws-lambda-go/cfn"
)

type ServiceLinkedRole struct {
	PhysicalResourceID string
	Data               map[string]interface{}

	event  cfn.Event
	status cfn.Response
}

func getPhysicalResourceID(event cfn.Event) string {
	if event.PhysicalResourceID != "" {
		return event.PhysicalResourceID
	}
	return custom.NewPhysicalResourceID(event)
}

func NewServiceLinkedRole(ctx context.Context, event cfn.Event) *ServiceLinkedRole {
	return &ServiceLinkedRole{
		PhysicalResourceID: getPhysicalResourceID(event),
		event:              event,
		status:             *cfn.NewResponse(&event),
	}
}

func (x ServiceLinkedRole) Create() (physicalResourceID string, data map[string]interface{}, err error) {
	w := &RoleWrapper{}
	role, err := w.CreateServiceLinkedRole()
}

func (x ServiceLinkedRole) Update() (physicalResourceID string, data map[string]interface{}, err error) {
	return x.PhysicalResourceID, x.Data, nil
}

func (x ServiceLinkedRole) Delete() (physicalResourceID string, data map[string]interface{}, err error) {
	return x.PhysicalResourceID, x.Data, nil
}

func (x ServiceLinkedRole) Handle() (physicalResourceID string, data map[string]interface{}, err error) {
	switch x.event.RequestType {
	case cfn.RequestCreate:
		return x.Create()
	case cfn.RequestUpdate:
		return x.Update()
	case cfn.RequestDelete:
		return x.Delete()
	}

	return
}
